# Coding Challenge

First, install docker and docker-compose, then:

Copy .env file `cp .env.example .env`

Run server `docker-compose up -d`

Composer install `docker-compose exec composer install`

Migrate and seed tables `docker-compose exec core php seed`

You can open dashboard: [http://127.0.0.1:7080](http://127.0.0.1:7080)

And phpMyAdmin: [http://127.0.0.1:7082](http://127.0.0.1:7082).(user: root, pass: secret)

# Notes
I did this challenge in `7` hours.

It works with SQLite and MySQL,  just change the `DB_CONNECTION=sqlite` to `DB_CONNECTION=mysql` in `.env` file, others env is ok by default.

All temporary data are located in `./storage`

Also, this project has CI. `.gitlab-ci.yml
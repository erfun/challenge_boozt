<?php

namespace System\Databases\Mysql;


use System\Databases\DB;

class Mysql implements DB
{
    /**
     * a PDO instance representing a connection to a database
     *
     * @var null
     */
    protected $db = null;


    /**
     * connect to the database
     *
     * @param        $dsn
     * @param string $user
     * @param string $pass
     * @return void
     */
    public function connect($dsn, string $user = '', string $pass = '')
    {
        $this->db = new \PDO($dsn, $user, $pass);

        // set the PDO error mode to exception
        $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }


    /**
     * Executes an SQL statement, returning a result set as a PDOStatement object
     *
     * @param string $query
     * @return \PDOStatement|false
     */
    public function query($query)
    {
        return $this->db->query($query);
    }


    /**
     * Prepares a statement for execution and returns a statement object
     *
     * @param string $query
     * @return \PDOStatement|false
     */
    public function prepare($query)
    {
        return $this->db->prepare($query);
    }
}
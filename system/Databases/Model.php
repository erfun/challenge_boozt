<?php

namespace System\Databases;


abstract class Model
{
    /**
     * Database connection
     *
     * @var Mysql\Mysql|Sqlite\Sqlite|null
     */
    private $database;


    /**
     * table name
     *
     * @var string
     */
    protected $table;


    /**
     * migration table
     *
     * @return void
     */
    abstract protected function createTable();


    /**
     * seed testing data for the table
     *
     * @return void
     */
    abstract public function seed();


    public function __construct()
    {
        if (getenv('DB_CONNECTION') == 'mysql') {
            $this->database = new Mysql\Mysql();
            $this->database->connect('mysql:host=' . getenv('DB_HOST') . ';dbname=' . getenv('DB_DATABASE') . ';port=' . getenv('DB_PORT'), getenv('DB_USERNAME'), getenv('DB_PASSWORD'));
        } else {
            $this->database = new Sqlite\Sqlite();
            $this->database->connect('sqlite:' . __DIR__ . '/../../storage/' . getenv('DB_DATABASE') . '.db');
        }

        if (!$this->tableExist())
            $this->createTable();
    }


    /**
     * Executes an SQL statement, returning a result set as a PDOStatement object
     *
     * @param string $query
     * @return \PDOStatement|false
     */
    public function query(string $query)
    {
        return $this->database->query($query);
    }


    /**
     * Prepares a statement for execution and returns a statement object
     *
     * @param string $query
     * @return \PDOStatement|false
     */
    public function prepare(string $query)
    {
        return $this->database->prepare($query);
    }


    /**
     * get the table name
     *
     * @return string
     */
    public function getTable(): string
    {
        return $this->table ?? strtolower(basename(str_replace('\\', '/', (is_object($this) ? get_class($this) : $this))));
    }


    /**
     * check the table exist
     *
     * @return bool
     */
    private function tableExist(): bool
    {
        try {
            $this->query("SELECT 1 FROM {$this->getTable()} LIMIT 1");
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }


    /**
     * return dateTime function for sqlite and mysql
     *
     * @param $field
     * @return string
     */
    protected function dateTimeFunction($field): string
    {
        return (getenv('DB_CONNECTION') == 'mysql' ? "DATE_FORMAT($field,'%Y-%m-%d')" : "strftime('%Y-%m-%d',$field)");
    }

    protected function getAutoIncrement()
    {
        return (getenv('DB_CONNECTION') == 'mysql' ? 'AUTO_INCREMENT' : 'AUTOINCREMENT');
    }
}
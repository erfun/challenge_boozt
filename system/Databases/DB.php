<?php

namespace System\Databases;


interface DB
{

    /**
     * connect to the database
     *
     * @param        $dsn
     * @param string $user
     * @param string $pass
     * @return void
     */
    public function connect($dsn, string $user = '', string $pass = '');


    /**
     * Executes an SQL statement, returning a result set as a PDOStatement object
     *
     * @param string $query
     * @return \PDOStatement|false
     */
    public function query(string $query);


    /**
     * Prepares a statement for execution and returns a statement object
     *
     * @param string $query
     * @return \PDOStatement|false
     */
    public function prepare(string $query);
}
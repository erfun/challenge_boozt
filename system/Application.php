<?php

namespace System;


class Application
{
    /**
     * The Router instance.
     *
     * @var Router
     */
    public $router;


    /**
     * Create a new application instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->bootstrapRouter();
    }

    /**
     * Bootstrap the router instance.
     *
     * @return void
     */
    public function bootstrapRouter()
    {
        $this->router = new Router($this);
    }
}
<?php

namespace System;


class Router
{

    /**
     * The application instance.
     *
     * @var Application
     */
    public $app;


    /**
     * All the routes waiting to be registered.
     *
     * @var array
     */
    protected $routes = [];


    /**
     * Router constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Register new route
     *
     * @param string $route
     * @param string $namespace
     * @param string $function
     * @param string $method
     */
    private function add(string $route, string $namespace, string $function, string $method)
    {
        $this->routes[strtolower($route . '-' . $method)] = ['namespace' => $namespace, 'function' => $function];
    }

    /**
     * Register new get method route
     *
     * @param string $route
     * @param string $namespace
     * @param string $function
     */
    public function get(string $route, string $namespace, string $function)
    {
        $this->add($route, $namespace, $function, 'get');
    }


    /**
     * Register new post method route
     *
     * @param string $route
     * @param string $namespace
     * @param string $function
     */
    public function post(string $route, string $namespace, string $function)
    {
        $this->add($route, $namespace, $function, 'post');
    }


    /**
     * Register All routes
     *
     * @param $callback
     * @return mixed|void
     */
    public function start($callback)
    {
        $callback($this);

        $currentRoute = strtolower(explode('?', $_SERVER['REQUEST_URI'], 2)[0] . '-' . $_SERVER['REQUEST_METHOD']);
        if (!isset($this->routes[$currentRoute])) {
            header("HTTP/1.1 404 Not Found");
            exit('404 Not Found');
        }

        if (!method_exists($this->routes[$currentRoute]['namespace'], $this->routes[$currentRoute]['function'])) {
            header("HTTP/1.1 400 method is not exist");
            exit('method is not exist');
        }

        $controller = new $this->routes[$currentRoute]['namespace'];
        return $controller->{$this->routes[$currentRoute]['function']}();
    }
}
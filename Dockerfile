FROM richarvey/nginx-php-fpm:1.10.3

RUN curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/bin/composer

WORKDIR /var/www/core
COPY . /var/www/core

RUN composer install --no-plugins --no-scripts --no-autoloader --prefer-dist --no-dev
RUN composer dump-autoload --optimize

COPY ./.env.example .env

RUN rm -Rf /etc/nginx/sites-enabled/default.conf
COPY default.conf /etc/nginx/sites-enabled/default.conf

RUN chmod -R o+w ./storage

CMD ["/start.sh"]

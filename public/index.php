<?php

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| First we need to get an application instance. This creates an instance
| of the application and bootstraps the application.
|
*/

$app = require __DIR__.'/../bootstrap/app.php';
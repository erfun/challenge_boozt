<?php

require_once __DIR__ . '/../vendor/autoload.php';


// load .env file
(new System\DotEnv(__DIR__ . '/../.env'))->load();

if (getenv('DEBUG') == 'true') {
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
}

// make a application object
$app = new \System\Application();


// start the router
$app->router->start(function ($router) {
    require __DIR__ . '/../app/routes.php';
});
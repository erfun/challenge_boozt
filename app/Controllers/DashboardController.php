<?php

namespace App\Controllers;


use App\Models\Customers;
use App\Models\OrderItems;
use App\Models\Orders;

final class DashboardController
{
    /**
     * it will return the dashboard
     */
    public function index()
    {
        // date validator
        if (isset($_GET['from']) && isset($_GET['to']) && $this->validateDate($_GET['from']) && $this->validateDate($_GET['to'])) {
            $startDate = $_GET['from'];
            $endDate = $_GET['to'];
        } else {
            $date = new \DateTime();
            $endDate = $date->format('Y-m-d');
            $date->sub(\DateInterval::createFromDateString('30 days'));
            $startDate = $date->format('Y-m-d');
        }

        { // get data from database
            $orderItemsModel = new OrderItems();
            $totalItem = $orderItemsModel->getTotalRevenue($startDate, $endDate);

            $ordersModel = new Orders();
            $totalOrdersList = $ordersModel->getTotalNewOrderPerDay($startDate, $endDate);
            $totalOrder = $ordersModel->getTotalOrder($startDate, $endDate);

            $customersModel = new Customers();
            $totalCustomersList = $customersModel->getTotalNewCustomerPerDay($startDate, $endDate);
            $totalCustomer = $customersModel->getTotalCustomer($startDate, $endDate);
        }

        // make CSV for high-chart
        $csvResult = "date,orders,customers\n";
        foreach ($totalOrdersList as $totalOrderList)
            $this->csvCompareDates($totalOrderList, $totalCustomersList, $csvResult);

        // return the HTML
        header("HTTP/1.1 200");
        include __DIR__ . '/../Views/index.php';
        return;
    }


    /**
     * Compare dates and make csv for high-chart
     *
     * @param $totalOrder
     * @param $totalCustomers
     * @param $csvResult
     */
    private function csvCompareDates($totalOrder, &$totalCustomers, &$csvResult)
    {
        if (!isset($totalCustomers[0])) {
            $csvResult .= $totalOrder['date'] . ',' . $totalOrder['total'] . ',0' . "\n";
            return;
        }
        if ($totalCustomers[0]['date'] < $totalOrder['date']) {
            $csvResult .= $totalCustomers[0]['date'] . ',0,' . $totalCustomers[0]['total'] . "\n";
            array_shift($totalCustomers);
            $this->csvCompareDates($totalOrder, $totalCustomers, $csvResult);
            return;
        }
        if ($totalCustomers[0]['date'] > $totalOrder['date']) {
            $csvResult .= $totalOrder['date'] . ',' . $totalOrder['total'] . ',0' . "\n";
            return;
        }
        if ($totalCustomers[0]['date'] == $totalOrder['date']) {
            $csvResult .= $totalOrder['date'] . ',' . $totalOrder['total'] . ',' . $totalCustomers[0]['total'] . "\n";
            array_shift($totalCustomers);
        }
    }


    /**
     * Date validator
     *
     * @param $date
     * @return false|int
     */
    private function validateDate($date)
    {
        return preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $date);
    }
}
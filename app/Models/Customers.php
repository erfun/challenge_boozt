<?php

namespace App\Models;


use System\Databases\Model;

class Customers extends Model
{
    /**
     * Get total new customers per day
     *
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    public function getTotalNewCustomerPerDay(string $startDate, string $endDate): array
    {
        return $this->query("SELECT COUNT(id) AS total, {$this->dateTimeFunction('register_date')} AS date from {$this->getTable()} WHERE {$this->dateTimeFunction('register_date')} >= '$startDate' AND {$this->dateTimeFunction('register_date')} <= '$endDate' GROUP BY {$this->dateTimeFunction('register_date')} ORDER BY register_date")
            ->fetchAll(2);
    }


    /**
     * Get total customers
     *
     * @param string $startDate
     * @param string $endDate
     * @return int
     */
    public function getTotalCustomer(string $startDate, string $endDate): int
    {
        return $this->query("SELECT COUNT(id) AS total from {$this->getTable()} WHERE {$this->dateTimeFunction('register_date')} >= '$startDate' AND {$this->dateTimeFunction('register_date')} <= '$endDate'")
                   ->fetchAll(2)[0]['total'];
    }


    /**
     * migration table
     *
     * @return void
     */
    protected function createTable()
    {
        $sql = "CREATE TABLE `{$this->getTable()}` (
                   `id`             INTEGER PRIMARY KEY {$this->getAutoIncrement()},
                   `first_name`     VARCHAR(20) NOT NULL,
                   `last_name`      VARCHAR(20) NOT NULL,
                   `register_date`  DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
                   `email`          VARCHAR(30) NOT NULL UNIQUE
            );";

        return$this->query($sql);
    }

    public function test(){
        return$this->createTable();
    }


    /**
     * seed testing data for the table
     *
     * @return void
     */
    public function seed(): void
    {
        $query = '';
        $data = [];
        for ($i = 0; $i < 60; $i++) {
            $date = new \DateTime();
            $date->sub(\DateInterval::createFromDateString(rand(30, 60) . ' days'));
            $data = array_merge($data, [$this->randomString(5), $this->randomString(5), $date->format('Y-m-d H:i:s'), $this->randomString() . '@gmail.com']);
            if ($query != '')
                $query .= ',';
            $query .= '(?, ?, ?, ?)';
        }

        for ($i = 0; $i < 100; $i++) {
            $date = new \DateTime();
            $date->sub(\DateInterval::createFromDateString(rand(0, 30) . ' days'));
            $data = array_merge($data, [$this->randomString(5), $this->randomString(5), $date->format('Y-m-d H:i:s'), $this->randomString() . '@gmail.com']);
            $query .= ', (?, ?, ?, ?)';
        }

        $this->prepare("INSERT INTO {$this->getTable()} (`first_name`, `last_name`, `register_date`, `email`) VALUES $query")->execute($data);
    }


    /**
     * generate random string
     *
     * @param int $length
     * @return string
     */
    private function randomString(int $length = 15): string
    {
        return substr(str_shuffle(str_repeat($x = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
    }
}
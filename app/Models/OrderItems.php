<?php

namespace App\Models;


use System\Databases\Model;

class OrderItems extends Model
{

    /**
     * Get total revenue
     *
     * @param string $startDate
     * @param string $endDate
     * @return int
     */
    public function getTotalRevenue(string $startDate, string $endDate): int
    {
        return $this->query("SELECT SUM({$this->getTable()}.price * {$this->getTable()}.quantity) AS total from {$this->getTable()} INNER JOIN orders ON orders.id={$this->getTable()}.order_id WHERE {$this->dateTimeFunction('orders.purchase_date')} >= '$startDate' AND {$this->dateTimeFunction('orders.purchase_date')} <= '$endDate'")
                      ->fetchAll(2)[0]['total'];
    }


    /**
     * migration table
     *
     * @return void
     */
    protected function createTable()
    {
        $sql = "CREATE TABLE `{$this->getTable()}` (
                   `id`             INTEGER PRIMARY KEY {$this->getAutoIncrement()},
                   `order_id`       INTEGER NOT NULL,
                   `price`          INTEGER NOT NULL,
                   `quantity`       INTEGER NOT NULL,
                   `ean`            VARCHAR(13) NOT NULL,
                   FOREIGN KEY (`order_id`) REFERENCES `orders`(`id`) ON DELETE CASCADE
            );";

        $this->query($sql);
    }


    /**
     * seed testing data for the table
     *
     * @return void
     */
    public function seed()
    {
        $prices = [15, 20, 45, 70, 110];

        $query = '';
        $data = [];
        for ($i = 1; $i <= 100; $i++) {
            $data = array_merge($data, [$i, $prices[rand(0, 4)], rand(1, 3), rand(1000000000000, 9999999999999)]);
            if ($query != '')
                $query .= ',';
            $query .= '(?, ?, ?, ?)';
        }

        for ($i = 1; $i <= 100; $i++) {
            $data = array_merge($data, [rand(100, 200), $prices[rand(0, 4)], rand(1, 3), rand(1000000000000, 9999999999999)]);
            $query .= ', (?, ?, ?, ?)';
        }

        $this->prepare("INSERT INTO {$this->getTable()} (`order_id`, `price`, `quantity`, `ean`) VALUES $query")->execute($data);
    }
}
<?php

namespace App\Models;

use System\Databases\Model;

class Orders extends Model
{

    /**
     * Get total new orders per day
     *
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    public function getTotalNewOrderPerDay(string $startDate, string $endDate): array
    {
        return $this->query("SELECT COUNT(id) AS total ,{$this->dateTimeFunction('purchase_date')} AS date from {$this->getTable()} WHERE {$this->dateTimeFunction('purchase_date')} >= '$startDate' AND {$this->dateTimeFunction('purchase_date')} <= '$endDate' GROUP BY {$this->dateTimeFunction('purchase_date')} ORDER BY purchase_date")
            ->fetchAll(2);
    }


    /**
     * Get total order
     *
     * @param string $startDate
     * @param string $endDate
     * @return int
     */
    public function getTotalOrder(string $startDate, string $endDate): int
    {
        return $this->query("SELECT COUNT(id) AS total from {$this->getTable()} WHERE {$this->dateTimeFunction('purchase_date')} >= '$startDate' AND {$this->dateTimeFunction('purchase_date')} <= '$endDate'")
                   ->fetchAll(2)[0]['total'];
    }


    /**
     * migration table
     *
     * @return void
     */
    protected function createTable()
    {
        $sql = "CREATE TABLE {$this->getTable()} (
                   id             INTEGER PRIMARY KEY {$this->getAutoIncrement()},
                   customer_id    INTEGER NOT NULL,
                   purchase_date  DATETIME NOT NULL,
                   country        VARCHAR(20) NOT NULL,
                   device         VARCHAR(20) NOT NULL,
                   FOREIGN KEY (customer_id) REFERENCES customers(id) ON DELETE CASCADE
            );";

        $this->query($sql);
    }


    /**
     * seed testing data for the table
     *
     * @return void
     */
    public function seed()
    {
        $countries = ['Sweden', 'Germany', 'France', 'Italy', 'Netherlands', 'Belgium', 'Denmark', 'Finland'];
        $devices = ['android', 'iphone', 'web', 'windows', 'linux', 'xos', 'other'];

        $query = '';
        $data = [];
        for ($i = 1; $i <= 60; $i++) {
            $date = new \DateTime();
            $date->sub(\DateInterval::createFromDateString(rand(0, 30) . ' days'));
            $data = array_merge($data, [$i, $date->format('Y-m-d H:i:s'), $countries[rand(0, 7)], $devices[rand(0, 6)]]);
            if ($query != '')
                $query .= ',';
            $query .= '(?, ?, ?, ?)';
        }

        for ($i = 1; $i <= 140; $i++) {
            $date = new \DateTime();
            $date->sub(\DateInterval::createFromDateString(rand(0, 30) . ' days'));
            $data = array_merge($data, [rand(1, 60), $date->format('Y-m-d H:i:s'), $countries[rand(0, 7)], $devices[rand(0, 6)]]);
            $query .= ', (?, ?, ?, ?)';
        }

        $this->prepare("INSERT INTO {$this->getTable()} (`customer_id`, `purchase_date`, `country`, `device`) VALUES $query")->execute($data);
    }
}